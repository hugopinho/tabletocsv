import csv
from decimal import Decimal
from tempfile import NamedTemporaryFile
import shutil
#Name column with html table
COLUMNAME = 'description'
#Name old csv file
filename = '2312AveiroCoimbra.csv'
tempfile = NamedTemporaryFile('w+t', newline='', delete=False)   

def getValue(string,inputSearch):
    x = string.split(inputSearch)
    if( len(x) > 1 ):
        y = x[1].split(' ')
        return y[0]
    else:
        return ''


with open(filename, 'r', newline='') as csvFile, tempfile:
    reader = csv.DictReader(csvFile)
    #Change names in array to your case 
    fieldnames = ['id','long', 'lat', 'alt', 'speed', 'heading', 'time']
    writer = csv.DictWriter(tempfile, fieldnames=fieldnames)
    writer.writeheader()
    count = 0

    #Loop in TD 
    #Change the string name for your case
    for row in reader:
        long = getValue(row[COLUMNAME],"Longitude: ")
        lat =  getValue(row[COLUMNAME],"Latitude: ")
        Altitude =  getValue(row[COLUMNAME],"Altitude: ")
        Speed =  getValue(row[COLUMNAME],"Speed: ")
        Heading =  getValue(row[COLUMNAME],"Heading: ")
        Time = getValue(row[COLUMNAME],"Time: ")
        mph = Decimal(1.609344)
        #convert speed in mph to Km/h
        if not Speed.strip():
            finalSpeed = 0
        else:
            finalSpeed = Decimal(Speed) * mph
            
        writer.writerow({'id':count,'long': long, 'lat': lat, 'alt': Altitude, 'speed': finalSpeed, 'heading': Heading, 'time': Time})
        count += 1

shutil.move(tempfile.name, "newCsv.csv")
   
