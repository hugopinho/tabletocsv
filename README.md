# Foobar

Html table to Csv is a Python script which turns an html table in csv file into a new csv with only table.


## Execution
```bash
    python main.pyt
```

## Contributing

    - Hugo Pinho

## License
[MIT](https://choosealicense.com/licenses/mit/)